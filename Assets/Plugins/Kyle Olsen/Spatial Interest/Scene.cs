﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Spatial
{
    public class Scene
    {
        public readonly IDivideSpace Space;

        private readonly StaticNodes staticNodes;
        private readonly DynamicNodes dynamicNodes;
        private Timed.TimerStopper mainThreadUpdate;

        public Scene(IDivideSpace space, float batchSizePercentage, int minBatchSize)
        {
            Space = space;
            staticNodes = new StaticNodes(QuerySpaceAndUpdateNode, minBatchSize, batchSizePercentage);
            dynamicNodes = new DynamicNodes(QuerySpaceAndUpdateNode, minBatchSize, batchSizePercentage);
            mainThreadUpdate = Timed.EveryFrame(MainThreadUpdate);
        }

        public void Add(INode node)
        {
            if (node == null)
            {
                return;
            }

            if (node.Static)
            {
                staticNodes.Add(node);
            }
            else
            {
                dynamicNodes.Add(node);
            }
        }

        public void Add(IEnumerable<INode> nodes)
        {
            Add(nodes as FastList<INode> ?? new FastList<INode>(nodes));
        }

        public void Add(FastList<INode> nodes)
        {
            dynamicNodes.Add(nodes);
            staticNodes.Add(nodes);
        }

        public void Remove(INode node)
        {
            Remove(NodeToEnumerable(node));
        }

        public void Remove(IEnumerable<INode> nodes)
        {
            staticNodes.Remove(nodes);
            dynamicNodes.Remove(nodes);
        }

        public void MarkStaticDirty()
        {
            staticNodes.MarkDirty();
        }

        public void TearDown()
        {
            try
            {
                Space?.Dispose();
                mainThreadUpdate.Stop();
                staticNodes.Dispose();
                dynamicNodes.Dispose();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void MainThreadUpdate()
        {
            staticNodes.MainThreadUpdate();
            dynamicNodes.MainThreadUpdate();
        }

        private void QuerySpaceAndUpdateNode(INode node)
        {
            var nodeCell = node.CurrentCell;
            var newCell = Space.QueryForCell(node.Position);

            if (newCell != nodeCell)
            {
                node.SetCell(newCell);
                nodeCell?.RemoveNode(node);
                newCell?.AddNode(node);
            }
        }

        private static IEnumerable<INode> NodeToEnumerable(INode node)
        {
            yield return node;
        }
    }
}