﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Spatial
{
    public class StaticNodes : IDisposable
    {
        private readonly Action<INode> queryNodeCallback;
        private readonly float batchSizePercentage;
        private readonly int minBatchSize;
        private readonly object staticNodeLock = new object();

        private readonly FastList<INode> staticNodes = new FastList<INode>(1000);
        private readonly ConcurrentQueue<INode> staticNodeQueue = new ConcurrentQueue<INode>();
        private WaitCallback threadStaticUpdateCallback;
        private volatile bool dirty, abortThreads;
        private int staticUpdateIndex, totalDirtyCountThisUpdate;

        public StaticNodes(Action<INode> queryNodeCallback, int minBatchSize, float batchSizePercentage)
        {
            this.queryNodeCallback = queryNodeCallback;
            this.minBatchSize = minBatchSize;
            this.batchSizePercentage = batchSizePercentage;

            threadStaticUpdateCallback = ThreadStaticWorkUpdate;
            new Thread(StaticThreadUpdate).Start();
        }

        public void MarkDirty()
        {
            dirty = true;
        }

        public void MainThreadUpdate()
        {
            if (dirty)
            {
                lock (staticNodeLock)
                {
                    var staticNodesCount = staticNodes.Count;

                    if (staticUpdateIndex >= staticNodesCount)
                    {
                        staticUpdateIndex = 0;
                        totalDirtyCountThisUpdate = 0;
                    }

                    bool finalBatch;
                    var batchSize = Utility.CalculateBatchSize(staticUpdateIndex, staticNodesCount, batchSizePercentage, minBatchSize);
                    var endIndex = staticUpdateIndex + batchSize;
                    
                    if (endIndex > staticNodesCount)
                    {
                        endIndex = staticNodesCount;
                        finalBatch = true;
                    }
                    else
                    {
                        finalBatch = false;
                    }

                    for (; staticUpdateIndex < endIndex; ++staticUpdateIndex)
                    {
                        if (!ValidNode(staticNodes[staticUpdateIndex]) || !staticNodes[staticUpdateIndex].Dirty)
                        {
                            continue;
                        }

                        ++totalDirtyCountThisUpdate;
                        
                        if (staticNodes[staticUpdateIndex].CurrentCell == null)
                        {
                            continue;
                        }

                        staticNodes[staticUpdateIndex].OnVisibleByChanged();
                    }
                    
                    if(finalBatch)
                    {
                        dirty = totalDirtyCountThisUpdate > 0;
                    }
                }
            }
        }

        private void StaticThreadUpdate()
        {
            while (!abortThreads)
            {
                var staticNodeCount = staticNodeQueue.Count;

                if (staticNodeCount > 0)
                {
                    var batchSize = Utility.CalculateBatchSize(0, staticNodeCount, batchSizePercentage, minBatchSize);

                    for (var startIndex = 0; startIndex < staticNodeCount; startIndex += batchSize)
                    {
                        if (abortThreads)
                        {
                            break;
                        }

                        if (batchSize >= staticNodeCount)
                        {
                            batchSize = staticNodeCount;
                        }

                        try
                        {
                            if (!ThreadPool.QueueUserWorkItem(threadStaticUpdateCallback, new StaticThreadData(batchSize)))
                            {
                                Debug.LogError("Failed to enqueue static thread work item");
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                }

                Thread.Sleep(100);
            }
        }

        private void ThreadStaticWorkUpdate(object state)
        {
            var data = (StaticThreadData)state;
            var remainingCount = data.Count;

            lock (staticNodeLock)
            {
                while (remainingCount > 0)
                {
                    try
                    {
                        INode node;

                        if (!staticNodeQueue.TryDequeue(out node) || node == null)
                        {
                            continue;
                        }

                        queryNodeCallback(node);
                        staticNodes.Add(node);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return;
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                    finally
                    {
                        --remainingCount;
                    }
                }
            }
        }

        public void Add(INode node)
        {
            staticNodeQueue.Enqueue(node);
        }

        public void Add(FastList<INode> nodes)
        {
            for (var i = 0; i < nodes.Count; ++i)
            {
                if (!ValidNode(nodes[i]) || !nodes[i].Static)
                {
                    continue;
                }

                staticNodeQueue.Enqueue(nodes[i]);
            }
        }

        public void Remove(IEnumerable<INode> nodes)
        {
            Remove(nodes as FastList<INode> ?? new FastList<INode>(nodes));
        }

        public void Remove(FastList<INode> nodes)
        {
            lock (staticNodeLock)
            {
                foreach (var node in nodes)
                {
                    if (!ValidNode(node))
                    {
                        continue;
                    }

                    if (staticNodes.Remove(node))
                    {
                        node.CurrentCell?.RemoveNode(node);
                        node.SetCell(null);
                    }
                }
            }
        }

        private static bool ValidNode(INode node)
        {
            if (node == null)
            {
                return false;
            }

            return (node as Component) != null;
        }

        private struct StaticThreadData
        {
            public StaticThreadData(int count)
            {
                Count = count;
            }

            public int Count { get; }
        }

        public void Dispose()
        {
            abortThreads = true;
        }
    }
}