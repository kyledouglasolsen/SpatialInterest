﻿namespace Spatial
{
    public static class Utility
    {
        public static int CalculateBatchSize(int currentIndex, int nodeCount, float batchSizePercentage, int minBatchSize = 10)
        {
            var batchSize = (int)(nodeCount * batchSizePercentage);

            if (batchSize < minBatchSize)
            {
                batchSize = minBatchSize;
            }

            var endIndex = currentIndex + batchSize;
            if (endIndex > nodeCount)
            {
                batchSize = endIndex - nodeCount;
            }

            return batchSize;
        }
    }
}