﻿namespace Spatial
{
    public class NeighborCell
    {
        public NeighborCell(ICell cell, int lodLevel)
        {
            Cell = cell;
            LodLevel = lodLevel;
        }

        public ICell Cell { get; }
        public int LodLevel { get; }
    }
}