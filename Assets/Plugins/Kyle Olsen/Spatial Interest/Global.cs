﻿using System;
using System.Threading;

namespace Spatial
{
    public static class Global
    {
        private static int registerCount;
        private static readonly object RegisterLock = new object();
        private static FastList<INode> registerList = new FastList<INode>(100);
        private static Timed.TimerStopper update;

        public static Scene Scene { get; private set; }
        public static IDivideSpace Space { get; private set; }

        public static void Initialize(IDivideSpace space, float batchSizePercentage = 0.1f, int minBatchSize = 10)
        {
            Space = space;
            Scene = new Scene(Space, batchSizePercentage, minBatchSize);
            update = Timed.EveryFrame(UpdateLoop);
        }

        /// <summary>
        /// Lock registration list and add node. Use this if you are registering nodes from many threads.
        /// </summary>
        /// <param name="node">Registering Node</param>
        public static void RegisterSafe(INode node)
        {
            lock (RegisterLock)
            {
                RegisterUnsafe(node);
            }
        }

        /// <summary>
        /// Register node without locking registration list. Use this if you never need to register nodes from other threads.
        /// </summary>
        /// <param name="node">Registering Node</param>
        public static void RegisterUnsafe(INode node)
        {
            registerList.Add(node);
            Interlocked.Increment(ref registerCount);
        }

        public static void UnRegister(INode node)
        {
            Scene?.Remove(node);

            if (registerCount > 0)
            {
                lock (RegisterLock)
                {
                    registerList.Remove(node);
                }
            }
        }

        public static void TearDown()
        {
            Interlocked.Exchange(ref registerCount, 0);

            try
            {
                Scene?.TearDown();
            }
            catch (Exception)
            {
                // ignore
            }

            try
            {
                registerList.Clear();
            }
            catch (Exception)
            {
                // ignore
            }

            update.Stop();
            Space = null;
        }

        private static void UpdateLoop()
        {
            if (registerCount > 0)
            {
                lock (RegisterLock)
                {
                    Scene.Add(registerList);
                    registerCount = 0;
                    registerList.Clear();
                }
            }
        }
    }
}