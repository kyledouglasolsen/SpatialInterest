﻿using System;
using UnityEngine;

namespace Spatial
{
    [Serializable]
    public class LodData
    {
        [SerializeField] private int[] lodRanges = {0, 1, 2, 3};

        public int Min => lodRanges[0];
        public int Max => lodRanges[lodRanges.Length - 1];
        public int Count => lodRanges.Length - 1;

        public int this[int index]
        {
            get { return lodRanges[index + 1]; }
            set { lodRanges[index + 1] = value; }
        }

        public void OnValidate()
        {
            if (lodRanges.Length < 1)
            {
                lodRanges = new int[1];
            }

            for (var i = 0; i < lodRanges.Length - 1; ++i)
            {
                lodRanges[i] = Mathf.Clamp(lodRanges[i], 0, int.MaxValue);

                if (lodRanges[i] >= lodRanges[i + 1])
                {
                    lodRanges[i + 1] = lodRanges[i] + 1;
                }
            }
        }

        public int GetLodLevelFromNeighborDistance(int neighborDistance)
        {
            for(var i = 0; i < lodRanges.Length; ++i)
            {
                if(lodRanges[i] >= neighborDistance)
                {
                    return i;
                }
            }
            
            return lodRanges.Length - 1;
        }
    }
}