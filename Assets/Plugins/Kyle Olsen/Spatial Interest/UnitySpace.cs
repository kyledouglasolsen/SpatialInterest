﻿using UnityEngine;

namespace Spatial
{
    public class UnitySpace : Singleton<UnitySpace>
    {
        [SerializeField] [Range(0f, 1f)] private float batchSizePercentage = 0.1f;
        [SerializeField] private int minBatchSize = 10;
        [SerializeField] private DivideSpaceData data = new DivideSpaceData();

        public IDivideSpaceData Data => data;

        protected override void OnAwake()
        {
            Timed.TryCreateInstance();
            Global.Initialize(new Grid(transform.localPosition, data), batchSizePercentage, minBatchSize);
        }

        private void OnDestroy()
        {
            Global.TearDown();
        }

#if UNITY_EDITOR

        private void OnValidate()
        {
            data.OnValidate();
        }
        
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            var halfCell = data.CellSize / 2f;
            var cellSize = new Vector3(data.CellSize, 0f, data.CellSize);

            for (var z = 0; z < data.GridDimension; ++z)
            {
                for (var x = 0; x < data.GridDimension; ++x)
                {
                    var bounds = new Bounds(transform.localPosition + new Vector3(x * data.CellSize + halfCell, 0f, z * data.CellSize + halfCell), cellSize);
                    Gizmos.DrawWireCube(bounds.center, bounds.size);
                }
            }

            if (Global.Space == null)
            {
                return;
            }

            for (var z = 0; z < data.GridDimension; ++z)
            {
                for (var x = 0; x < data.GridDimension; ++x)
                {
                    var index = x + z * data.GridDimension;
                    var cell = Global.Space.CellByIndex(index);

                    if (cell.LocalNodeCount == 0)
                    {
                        continue;
                    }

                    Gizmos.color = Color.red;
                    cell.OnDrawGizmos();
                }
            }
        }

#endif
    }
}