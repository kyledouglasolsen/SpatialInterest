﻿using System;
using UnityEngine;

namespace Spatial
{
    [Serializable]
    public class DivideSpaceData : IDivideSpaceData
    {
        [SerializeField] private int gridDimension = 10;
        [SerializeField] private int cellSize = 10;
        [SerializeField] private LodData lodData = new LodData();
        
        public int GridDimension => gridDimension;
        public int CellSize => cellSize;
        public LodData Lod => lodData;

        public void OnValidate()
        {
        }
    }
}