﻿using System;
using System.Threading;
using UnityEngine;

namespace Spatial
{
    public delegate void VisibleChanged(VisibleByList visibleBy);

    public delegate void LodIndexChanged(int lodIndex);

    public class VisibleNode : MonoBehaviour, INode
    {
        [SerializeField] private volatile bool autoRegister = true;
        [SerializeField] private volatile bool isStatic;
        private ICell currentCell;
        private Transform cachedTransform;
        private LodLevelCounter lodLevelCounter;
        private int dirtyCount;
        private int newLodIndex = -1;
        private int currentLodIndex = -1;
        private bool woke;
        private readonly object currentCellLock = new object();

        public bool Dirty => dirtyCount > 0;

        public bool AutoRegister
        {
            get { return autoRegister; }
            set { autoRegister = value; }
        }

        public bool Static
        {
            get { return isStatic; }
            set { isStatic = value; }
        }

        public INodeComponents Components { get; private set; }

        public ICell CurrentCell
        {
            get
            {
                lock (currentCellLock)
                {
                    return currentCell;
                }
            }
        }

        public Vector3 Position { get; private set; }
        public Quaternion Rotation { get; private set; }
        public int CurrentLodIndex => currentLodIndex;

        public event VisibleChanged OnVisibleByChangedEvent;
        public event LodIndexChanged OnLodIndexChangedEvent;

        public void SetCell(ICell cell)
        {
            lock (currentCellLock)
            {
                currentCell = cell;
            }
        }

        public void UpdateData()
        {
            if (cachedTransform.hasChanged)
            {
                Position = cachedTransform.position;
                Rotation = cachedTransform.rotation;
                cachedTransform.hasChanged = false;
            }
        }

        public void OnViewerAdd(ViewerReference viewerReference)
        {
            var index = lodLevelCounter.Increment(viewerReference.NeighborCell.LodLevel);
            Interlocked.Exchange(ref newLodIndex, index);
            MarkDirty();
        }

        public void OnViewerRemove(ViewerReference viewerReference)
        {
            var index = lodLevelCounter.Decrement(viewerReference.NeighborCell.LodLevel);
            Interlocked.Exchange(ref newLodIndex, index);
            MarkDirty();
        }

        public void OnVisibleByChanged()
        {
            try
            {
                var oldCurrentLod = Interlocked.Exchange(ref currentLodIndex, newLodIndex);

                if (oldCurrentLod != currentLodIndex)
                {
                    OnLodIndexChangedEvent?.Invoke(currentLodIndex);
                }

                var visibleByChanged = OnVisibleByChangedEvent;

                if (visibleByChanged == null)
                {
                    return;
                }

                var cell = CurrentCell;

                if (cell != null)
                {
                    using (var visibleBy = cell.GetVisibleByFromCache())
                    {
                        visibleByChanged(visibleBy);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                Interlocked.Decrement(ref dirtyCount);
            }
        }

        public void Register()
        {
            if (autoRegister)
            {
                return;
            }

            UpdateData();
            Global.RegisterUnsafe(this);
        }

        public void UnRegister()
        {
            if (autoRegister)
            {
                return;
            }

            Global.UnRegister(this);
        }

        private void Start()
        {
            lodLevelCounter = new LodLevelCounter(Global.Space.MaxNeighborRange + 1);
            cachedTransform = cachedTransform ?? GetComponent<Transform>();
            Components = GetComponent<INodeComponents>();
        
            if (autoRegister)
            {
                UpdateData();
                Global.RegisterUnsafe(this);
            }
            
            woke = true;
        }

        private void OnEnable()
        {
            if (woke && autoRegister)
            {
                UpdateData();
                Global.RegisterUnsafe(this);
            }
        }

        private void OnDisable()
        {
            if (autoRegister)
            {
                Global.UnRegister(this);
            }
        }

        private void MarkDirty()
        {
            Interlocked.Increment(ref dirtyCount);

            if (Static)
            {
                Global.Scene.MarkStaticDirty();
            }
        }
    }
}