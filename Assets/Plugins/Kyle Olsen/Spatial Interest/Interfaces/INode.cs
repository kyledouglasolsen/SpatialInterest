﻿using UnityEngine;

namespace Spatial
{
    public interface INode
    {
        bool Dirty { get; }
        bool AutoRegister { get; set; }
        bool Static { get; set; }
        INodeComponents Components { get; }

        ICell CurrentCell { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        int CurrentLodIndex { get; }

        event VisibleChanged OnVisibleByChangedEvent;
        event LodIndexChanged OnLodIndexChangedEvent;

        void Register();
        void UnRegister();

        void SetCell(ICell cell);
        void UpdateData();

        void OnViewerAdd(ViewerReference viewerReference);
        void OnViewerRemove(ViewerReference viewerReference);
        void OnVisibleByChanged();
    }
}