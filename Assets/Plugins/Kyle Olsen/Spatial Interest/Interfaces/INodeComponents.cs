﻿namespace Spatial
{
    public interface INodeComponents
    {
        void Add<T>(T instance) where T : class;
        T Get<T>() where T : class;
    }
}