﻿using System;
using UnityEngine;

namespace Spatial
{
    public interface IDivideSpace : IDisposable
    {
        Vector3 Center { get; }
        IDivideSpaceData Data { get; }
        int MaxNeighborRange { get; }

        ICell CellByIndex(int index);
        ICell QueryForCell(Vector3 position, bool clamp = true);
    }
}