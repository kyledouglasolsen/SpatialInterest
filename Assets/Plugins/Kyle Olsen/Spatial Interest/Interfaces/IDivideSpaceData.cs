﻿namespace Spatial
{
    public interface IDivideSpaceData
    {
        int GridDimension { get; }
        int CellSize { get; }
        LodData Lod { get; }
    }
}