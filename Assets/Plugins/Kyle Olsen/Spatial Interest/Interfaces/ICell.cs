﻿using System.Collections.Generic;
using UnityEngine;

namespace Spatial
{
    public interface ICell
    {
        int Index { get; }
        IDivideSpace Space { get; }
        Vector3 Center { get; }
        int LocalNodeCount { get; }

        IEnumerable<NeighborCell> Neighbors { get; }
        void SetNeighbors(IEnumerable<NeighborCell> cells);

        void AddNode(INode node);
        void RemoveNode(INode node);

        void OnNeighborAddedViewer(IViewerNode viewerNode, ICell neighbor);
        void OnNeighborRemovedViewer(IViewerNode viewerNode, ICell neighbor);

        VisibleByList GetVisibleByFromCache();

        void OnDrawGizmos();
    }
}