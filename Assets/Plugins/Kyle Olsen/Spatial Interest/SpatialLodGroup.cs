﻿using System;
using System.Linq;
using UnityEngine;

namespace Spatial
{
    public class SpatialLodGroup : MonoBehaviour
    {
        [SerializeField] private LodRenderer[] renderers = new LodRenderer[0];
        [SerializeField] [HideInInspector] private SpatialBehaviour spatialBehaviour;
        private int lodCount;
        private bool woke;

        public INode Node { get; private set; }

        public void RefreshLodMeshes()
        {
            RefreshLodMeshes(Node.CurrentLodIndex);
        }

        public void RefreshLodMeshes(int lodLevel)
        {
            for (var i = 0; i < lodCount; ++i)
            {
                if (renderers[i].HasRenderer)
                {
                    renderers[i].Renderer.enabled = lodLevel == i;
                }
            }
        }

        private void Start()
        {
            Node = GetComponentInParent<INode>();
            lodCount = Mathf.Min(Global.Space.Data.Lod.Max, renderers.Length);

            for (var i = 0; i < lodCount; ++i)
            {
                renderers[i].Start();
            }

            Initialize();
            woke = true;
        }

        private void OnEnable()
        {
            if (woke)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            if (Node != null)
            {
                Node.OnLodIndexChangedEvent += LodIndexUpdated;
            }
        }

        private void OnDisable()
        {
            if (Node != null)
            {
                Node.OnLodIndexChangedEvent -= LodIndexUpdated;
            }
        }

        private void LodIndexUpdated(int lodIndex)
        {
            RefreshLodMeshes(lodIndex);
        }

        [Serializable]
        private class LodRenderer
        {
            [SerializeField] private Renderer renderer;
            private bool hasRenderer;

            public LodRenderer(Renderer renderer)
            {
                this.renderer = renderer;
                Start();
            }

            public Renderer Renderer => renderer;
            public bool HasRenderer => hasRenderer;

            public void Start()
            {
                hasRenderer = renderer != null;
            }
        }

#if UNITY_EDITOR

        private void Reset()
        {
            spatialBehaviour = this.GetOrAddComponent<SpatialBehaviour>();
            var foundRenderers = GetComponentsInChildren<Renderer>(true);
            renderers = new LodRenderer[foundRenderers.Length];

            for (var i = 0; i < renderers.Length; ++i)
            {
                renderers[i] = new LodRenderer(foundRenderers[i]);
            }

            renderers = renderers.OrderBy(x => x.Renderer.name).ToArray();
        }

        private void OnValidate()
        {
            if (Application.isPlaying)
            {
                return;
            }

            if (spatialBehaviour == null)
            {
                spatialBehaviour = this.GetOrAddComponent<SpatialBehaviour>();
            }

            for (var i = 0; i < renderers.Length; ++i)
            {
                if (renderers[i].Renderer == null)
                {
                    continue;
                }

                var nowEnabled = i + 1 >= renderers.Length;

                if (renderers[i].Renderer.enabled != nowEnabled)
                {
                    renderers[i].Renderer.enabled = nowEnabled;
                }
            }
        }

#endif
    }
}