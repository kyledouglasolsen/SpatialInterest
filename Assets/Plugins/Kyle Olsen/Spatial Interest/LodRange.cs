﻿using System;
using UnityEngine;

namespace Spatial
{
    [Serializable]
    public class LodRange
    {
        [SerializeField] private int min;
        [SerializeField] private int max;

        public LodRange(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public int Min => min;
        public int Max => max;

        public void SetMin(int newMin)
        {
            min = newMin;
        }

        public void SetMax(int newMax)
        {
            max = newMax;
        }
    }
}