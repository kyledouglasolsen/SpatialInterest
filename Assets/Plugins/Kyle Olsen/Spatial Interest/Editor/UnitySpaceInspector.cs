﻿using UnityEditor;
using UnityEngine;

namespace Spatial
{
    [CustomEditor(typeof(UnitySpace))]
    public class UnitySpaceInspector : Editor
    {
        private UnitySpace space;
        private Color[] colors;

        private void OnEnable()
        {
            space = (UnitySpace)target;

            if (colors == null)
            {
                colors = new Color[100];
                for (var i = 0; i < colors.Length; ++i)
                {
                    colors[i] = new Color(Random.value, Random.value, Random.value, 0.1f);
                }
            }
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();
            
            var lod = space.Data.Lod;
            
            if(EditorGUI.EndChangeCheck())
            {
                lod.OnValidate();
            }

            const float height = 30f;
            var previousX = 0f;
            var previousValue = 0;
            var rect = GUILayoutUtility.GetRect(Screen.width, height);

            for (var i = 0; i < lod.Count; ++i)
            {
                var size = lod[i] - previousValue;
                var pct = size / (float)lod.Max;
                var width = Screen.width * pct;
                var lodRect = new Rect(previousX, rect.y, width, height);
                EditorGUI.DrawRect(lodRect, colors[i]);
                previousX += width;
                EditorGUI.LabelField(lodRect, i.ToString());
                previousValue = lod[i];
            }
        }
    }
}