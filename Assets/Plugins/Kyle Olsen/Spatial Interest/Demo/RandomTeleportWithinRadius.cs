﻿using Spatial;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomTeleportWithinRadius : MonoBehaviour, IResume
{
    [SerializeField] private float maxRadius = 50f;
    private Vector3 startPosition;

    public void SetMaxRadius(float newMaxRadius)
    {
        maxRadius = newMaxRadius;
    }

    public void PickNewPosition()
    {
        var random = Random.insideUnitCircle * maxRadius;
        transform.localPosition = startPosition + new Vector3(random.x, 0f, random.y);
    }
    
    public void OnResume(float pausedSeconds)
    {
        if (startPosition == Vector3.zero)
        {
            startPosition = transform.localPosition;
        }

        PickNewPosition();
    }
}