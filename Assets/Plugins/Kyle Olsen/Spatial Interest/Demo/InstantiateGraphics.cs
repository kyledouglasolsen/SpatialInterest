﻿using Spatial;
using UnityEngine;

public class InstantiateGraphics : MonoBehaviour
{
    [SerializeField] private GameObject graphics = null;
    private INode node;

    private void Start()
    {
        node = GetComponent<INode>();
        Instantiate(graphics, transform.position, Quaternion.identity, transform);
        node.Register();
    }

    private void OnDestroy()
    {
        if (node != null)
        {
            node.UnRegister();
        }
    }
}