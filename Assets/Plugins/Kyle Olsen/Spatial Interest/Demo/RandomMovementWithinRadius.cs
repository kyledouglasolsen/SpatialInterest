﻿using System;
using Spatial;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomMovementWithinRadius : MonoBehaviour, IResume, IUpdate
{
    [SerializeField] private float maxRadius = 50f;
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float stopDistance = 0.1f;
    private Vector3 startPosition, targetPosition, deltaMove;
    private float previousSqrDistance;

    public bool IsMoving { get; private set; }
    public event Action<bool> IsMovingChangedEvent = delegate { };

    public bool Enabled { get; private set; }

    public void SetMaxRadius(float newMaxRadius)
    {
        maxRadius = newMaxRadius;
    }

    public void SetMoveSpeed(float newMoveSpeed)
    {
        moveSpeed = newMoveSpeed;
    }

    public void SetStopDistance(float newStopDistance)
    {
        stopDistance = newStopDistance;
    }

    public void PickNewPosition()
    {
        startPosition = transform.localPosition;
        var random = Random.insideUnitCircle * maxRadius;
        targetPosition = startPosition + new Vector3(random.x, 0f, random.y);
        deltaMove = (targetPosition - startPosition).normalized;
        transform.rotation = Quaternion.LookRotation(deltaMove);
        deltaMove *= moveSpeed;

        if (IsMoving != true)
        {
            IsMoving = true;
            IsMovingChangedEvent(true);
        }
    }

    public void OnUpdate()
    {
        var pos = transform.localPosition;
        pos += deltaMove * Time.deltaTime;
        var sqrDistance = (pos - targetPosition).sqrMagnitude;
        var nowIsMoving = sqrDistance > stopDistance && sqrDistance < previousSqrDistance;
        previousSqrDistance = sqrDistance;

        if (nowIsMoving)
        {
            transform.localPosition = pos;
        }
        else
        {
            PickNewPosition();
        }

        if (IsMoving != nowIsMoving)
        {
            IsMoving = nowIsMoving;
            IsMovingChangedEvent(IsMoving);
        }
    }

    public void OnResume(float pausedSeconds)
    {
        PickNewPosition();
    }

    private void OnEnable()
    {
        Enabled = true;
    }

    private void OnDisable()
    {
        Enabled = false;
    }
}