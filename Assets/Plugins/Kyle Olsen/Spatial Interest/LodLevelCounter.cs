﻿using System;
using System.Threading;

namespace Spatial
{
    [Serializable]
    public class LodLevelCounter
    {
        private ReaderWriterLockSlim locker = new ReaderWriterLockSlim();
        private LodLevelCount[] visibleByLodLevels;
        private volatile int currentLodIndex = -1;

        public LodLevelCounter(int lodLevels)
        {
            visibleByLodLevels = new LodLevelCount[lodLevels];

            for (var i = 0; i < visibleByLodLevels.Length; ++i)
            {
                visibleByLodLevels[i] = new LodLevelCount();
            }
        }

        public int CurrentLodIndex => currentLodIndex;

        public int Increment(int lodLevel)
        {
            locker.EnterReadLock();

            try
            {
                visibleByLodLevels[lodLevel].Increment();
                return currentLodIndex = GetCurrentLodIndex();
            }
            finally
            {
                locker.ExitReadLock();
            }
        }

        public int Decrement(int lodLevel)
        {
            locker.EnterReadLock();

            try
            {
                visibleByLodLevels[lodLevel].Decrement();
                return currentLodIndex = GetCurrentLodIndex();
            }
            finally
            {
                locker.ExitReadLock();
            }
        }

        private int GetCurrentLodIndex()
        {
            for (var i = 0; i < visibleByLodLevels.Length; ++i)
            {
                if (visibleByLodLevels[i].Value > 0)
                {
                    return i;
                }
            }

            return visibleByLodLevels.Length - 1;
        }

        public void ClearAll()
        {
            locker.EnterReadLock();

            try
            {
                if (visibleByLodLevels != null)
                {
                    for (var i = 0; i < visibleByLodLevels.Length; ++i)
                    {
                        visibleByLodLevels[i].Clear();
                    }
                }
            }
            finally
            {
                locker.ExitReadLock();
            }
        }

        [Serializable]
        private class LodLevelCount
        {
            private int value;
            public int Value => value;

            public void Increment()
            {
                Interlocked.Increment(ref value);
            }

            public void Decrement()
            {
                Interlocked.Decrement(ref value);
            }

            public void Clear()
            {
                Interlocked.Exchange(ref value, 0);
            }
        }
    }
}