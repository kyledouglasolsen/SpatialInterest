﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Spatial
{
    public class DynamicNodes : IDisposable
    {
        private readonly Action<INode> queryNodeCallback;
        private readonly ReaderWriterLockSlim dynamicNodeLock = new ReaderWriterLockSlim();
        private readonly float batchSizePercentage;
        private readonly int minBatchSize;
        private volatile bool abortThreads;
        private int dynamicUpdateIndex, activeDynamicThreads;

        private WaitCallback threadDynamicUpdateCallback;
        private readonly FastList<INode> dynamicNodes = new FastList<INode>(10);
        private volatile int dynamicNodeCount;

        public DynamicNodes(Action<INode> queryNodeCallback, int minBatchSize, float batchSizePercentage)
        {
            this.queryNodeCallback = queryNodeCallback;
            this.minBatchSize = minBatchSize;
            this.batchSizePercentage = batchSizePercentage;
            threadDynamicUpdateCallback = ThreadDynamicWorkUpdate;

            new Thread(DynamicThreadUpdate).Start();
        }

        public void MainThreadUpdate()
        {
            if (dynamicNodeCount < 1)
            {
                return;
            }

            if (dynamicUpdateIndex >= dynamicNodeCount)
            {
                dynamicUpdateIndex = 0;
            }

            var batchSize = Utility.CalculateBatchSize(dynamicUpdateIndex, dynamicNodeCount, batchSizePercentage, minBatchSize);
            var endIndex = dynamicUpdateIndex + batchSize;

            dynamicNodeLock.EnterReadLock();

            try
            {
                for (var i = dynamicUpdateIndex; i < endIndex; ++i)
                {
                    if (i >= dynamicNodeCount)
                    {
                        break;
                    }

                    if (!ValidNode(dynamicNodes[i]))
                    {
                        continue;
                    }

                    try
                    {
                        dynamicNodes[i].UpdateData();

                        if (dynamicNodes[i].Dirty && dynamicNodes[i].CurrentCell != null)
                        {
                            dynamicNodes[i].OnVisibleByChanged();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
            finally
            {
                dynamicNodeLock.ExitReadLock();
            }

            dynamicUpdateIndex += batchSize;
        }

        public void Add(INode node)
        {
            dynamicNodeLock.EnterWriteLock();

            try
            {
                dynamicNodes.Add(node);
            }
            finally
            {
                dynamicNodeCount = dynamicNodes.Count;
                dynamicNodeLock.ExitWriteLock();
            }
        }

        public void Add(FastList<INode> nodes)
        {
            dynamicNodeLock.EnterWriteLock();

            try
            {
                for (var i = nodes.Count - 1; i >= 0; --i)
                {
                    if (!ValidNode(nodes[i]) || nodes[i].Static)
                    {
                        continue;
                    }

                    dynamicNodes.Add(nodes[i]);
                    nodes.RemoveAt(i);
                }
            }
            finally
            {
                dynamicNodeCount = dynamicNodes.Count;
                dynamicNodeLock.ExitWriteLock();
            }
        }

        public void Remove(IEnumerable<INode> nodes)
        {
            dynamicNodeLock.EnterWriteLock();

            try
            {
                foreach (var node in nodes)
                {
                    if (!ValidNode(node))
                    {
                        continue;
                    }

                    node.CurrentCell?.RemoveNode(node);
                    node.SetCell(null);
                    dynamicNodes.Remove(node);
                }

                dynamicNodeCount = dynamicNodes.Count;
            }
            finally
            {
                dynamicNodeLock.ExitWriteLock();
            }
        }

        private void DynamicThreadUpdate()
        {
            while (!abortThreads)
            {
                if (dynamicNodeCount > 0)
                {
                    var batchSize = Utility.CalculateBatchSize(0, dynamicNodeCount, batchSizePercentage, minBatchSize);

                    for (var startIndex = 0; startIndex < dynamicNodeCount; startIndex += batchSize)
                    {
                        if (abortThreads)
                        {
                            break;
                        }

                        var endIndex = startIndex + batchSize;

                        if (endIndex > dynamicNodeCount)
                        {
                            endIndex = dynamicNodeCount;
                        }

                        try
                        {
                            if (ThreadPool.QueueUserWorkItem(threadDynamicUpdateCallback, new DynamicThreadData(startIndex, endIndex)))
                            {
                                Interlocked.Increment(ref activeDynamicThreads);
                            }
                            else
                            {
                                Debug.LogError("Failed to enqueue dynamic work item");
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                }

                do
                {
                    if (abortThreads)
                    {
                        break;
                    }

                    Thread.Sleep(1);
                } while (activeDynamicThreads > 0);
            }
        }

        private void ThreadDynamicWorkUpdate(object state)
        {
            var data = (DynamicThreadData)state;

            dynamicNodeLock.EnterReadLock();

            try
            {
                for (var i = data.StartIndex; i < data.EndIndex; ++i)
                {
                    try
                    {
                        if (abortThreads || i >= dynamicNodeCount)
                        {
                            break;
                        }

                        var node = dynamicNodes[i];

                        if (!ValidNode(node))
                        {
                            continue;
                        }

                        queryNodeCallback(node);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
            finally
            {
                Interlocked.Decrement(ref activeDynamicThreads);
                dynamicNodeLock.ExitReadLock();
            }
        }

        public void Dispose()
        {
            dynamicNodes.Clear();
            abortThreads = true;
        }

        private static bool ValidNode(INode node)
        {
            if (node == null)
            {
                return false;
            }

            return (node as Component) != null;
        }

        private struct DynamicThreadData
        {
            public DynamicThreadData(int startIndex, int endIndex) : this()
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
            }

            public int StartIndex { get; }
            public int EndIndex { get; }
        }
    }
}