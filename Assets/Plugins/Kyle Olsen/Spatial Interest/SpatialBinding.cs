﻿using System;

namespace Spatial
{
    [Serializable]
    public class SpatialBinding
    {
        private readonly FastList<ILifetimeCallback> callbacks = new FastList<ILifetimeCallback>();
        private volatile bool updating;
        private INode node;

        public SpatialBinding(INode node)
        {
            Updater.Initialize();
            this.node = node;
        }

        public bool Updating => updating;

        public void RegisterComponentForUpdates(ILifetimeCallback callback)
        {
            if (callback == null || callbacks == null)
            {
                return;
            }

            if (callbacks.TryAdd(callback))
            {
                if (updating)
                {
                    Updater.Register(callback);
                }
                else
                {
                    Updater.UnRegister(callback);
                }
            }
        }

        public void RemoveComponentFromUpdates(ILifetimeCallback callback)
        {
            if (callback == null || callbacks == null)
            {
                return;
            }

            if (callbacks.Remove(callback))
            {
                if (updating)
                {
                    Updater.UnRegister(callback);
                }
            }
        }

        public void Enable()
        {
            if (node != null)
            {
                node.OnLodIndexChangedEvent += OnNodesChanged;
            }
        }

        public void Disable()
        {
            if (updating)
            {
                for (var i = 0; i < callbacks.Count; ++i)
                {
                    if (callbacks[i] != null)
                    {
                        Updater.UnRegister(callbacks[i]);
                    }
                }

                updating = false;
            }

            if (node != null)
            {
                node.OnLodIndexChangedEvent -= OnNodesChanged;
            }
        }

        private void OnNodesChanged(int lodIndex)
        {
            var nowUpdating = lodIndex < Global.Scene.Space.MaxNeighborRange;

            if (updating == nowUpdating)
            {
                return;
            }

            updating = nowUpdating;

            for (var i = 0; i < callbacks.Count; ++i)
            {
                if (callbacks[i] == null)
                {
                    continue;
                }

                if (nowUpdating)
                {
                    Updater.Register(callbacks[i]);
                }
                else
                {
                    Updater.UnRegister(callbacks[i]);
                }
            }
        }
    }
}