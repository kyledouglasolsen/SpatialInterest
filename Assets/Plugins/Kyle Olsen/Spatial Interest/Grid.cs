﻿using System.Collections.Generic;
using UnityEngine;

namespace Spatial
{
    public class Grid : IDivideSpace
    {
        private static readonly List<NeighborCell> NeighborCache = new List<NeighborCell>();

        private readonly GridCell[] cells;

        public Grid(Vector3 center, IDivideSpaceData data)
        {
            Center = center;
            Data = data;
            MaxNeighborRange = Data.Lod.Max;
            cells = new GridCell[data.GridDimension * data.GridDimension];

            var halfCell = data.CellSize / 2f;

            var index = 0;

            for (var z = 0; z < data.GridDimension; ++z)
            {
                for (var x = 0; x < data.GridDimension; ++x)
                {
                    var position = center + new Vector3(x * data.CellSize + halfCell, 0f, z * data.CellSize + halfCell);
                    cells[index] = new GridCell(index, this, position);
                    ++index;
                }
            }

            for (var z = 0; z < data.GridDimension; ++z)
            {
                for (var x = 0; x < data.GridDimension; ++x)
                {
                    var currentIndex = GetIndex(x, z);
                    var currentCell = cells[currentIndex];
                    CalculateNeighborCells(currentCell, x, z, data);
                }
            }
        }

        public Vector3 Center { get; }
        public IDivideSpaceData Data { get; }
        public int MaxNeighborRange { get; }

        public ICell CellByIndex(int index)
        {
            return cells[index];
        }

        public ICell QueryForCell(Vector3 position, bool clamp = true)
        {
            var x = (int)((position.x - Center.x) / Data.CellSize);
            var z = (int)((position.z - Center.z) / Data.CellSize);

            if (clamp)
            {
                x = Mathf.Clamp(x, 0, Data.GridDimension - 1);
                z = Mathf.Clamp(z, 0, Data.GridDimension - 1);
            }

            return CellByIndex(GetIndex(x, z));
        }

        private int GetIndex(int x, int z)
        {
            return x + z * Data.GridDimension;
        }

        private void CalculateNeighborCells(ICell currentCell, int x, int z, IDivideSpaceData data)
        {
            var range = Mathf.Max(MaxNeighborRange, 1);
            var leftX = x - range;
            var leftZ = z + range;
            var rightX = x + range;
            var rightZ = z - range;

            if (leftX < 0)
            {
                leftX = 0;
            }

            if (leftZ >= Data.GridDimension)
            {
                leftZ = Data.GridDimension - 1;
            }

            if (rightX >= Data.GridDimension)
            {
                rightX = Data.GridDimension - 1;
            }

            if (rightZ < 0)
            {
                rightZ = 0;
            }

            for (var offsetZ = leftZ; offsetZ >= rightZ; --offsetZ)
            {
                for (var offsetX = leftX; offsetX <= rightX; ++offsetX)
                {
                    var index = GetIndex(offsetX, offsetZ);
                    var neighborCell = cells[index];
                    var lodX = x - offsetX;
                    var lodZ = z - offsetZ;

                    if (lodX < 0)
                    {
                        lodX = -lodX;
                    }

                    if (lodZ < 0)
                    {
                        lodZ = -lodZ;
                    }

                    NeighborCache.Add(new NeighborCell(neighborCell, data.Lod.GetLodLevelFromNeighborDistance(lodX > lodZ ? lodX : lodZ)));
                }
            }

            currentCell.SetNeighbors(NeighborCache);
            NeighborCache.Clear();
        }

        public void Dispose()
        {
            if (cells != null)
            {
                for (var i = 0; i < cells.Length; ++i)
                {
                    cells[i]?.Dispose();
                }
            }
        }
    }
}