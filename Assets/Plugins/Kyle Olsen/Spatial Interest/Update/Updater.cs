﻿using System;
using UnityEngine;

namespace Spatial
{
    public class Updater : Singleton<Updater>
    {
        private static volatile bool initialized;

        private static readonly object UpdatesLock = new object();
        private static readonly FastList<IUpdate> Updates = new FastList<IUpdate>(100);

        private static readonly object FixedUpdatesLock = new object();
        private static readonly FastList<IFixedUpdate> FixedUpdates = new FastList<IFixedUpdate>(100);

        private static readonly object LateUpdatesLock = new object();
        private static readonly FastList<ILateUpdate> LateUpdates = new FastList<ILateUpdate>(100);

        private static readonly object ResumeLock = new object();
        private static readonly FastList<IResume> Resumes = new FastList<IResume>(100);

        private static readonly object PauseLock = new object();
        private static readonly FastList<IPause> Pauses = new FastList<IPause>(100);

        public static float Time { get; private set; }

        public static void Initialize()
        {
            if (initialized)
            {
                return;
            }

            var instance = TryCreateInstance();

            if (instance != null)
            {
                initialized = true;
                Time = UnityEngine.Time.time;
            }
            else
            {
                Debug.LogError("Failed to initialize Spatial.Updater!");
            }
        }

        public static void Register(object target)
        {
            if (target == null)
            {
                return;
            }

            var ins = Ins;

            if (ins == null)
            {
                return;
            }

            var update = target as IUpdate;
            if (update != null)
            {
                lock (UpdatesLock)
                {
                    Updates.Add(update);
                }
            }

            var fixedUpdate = target as IFixedUpdate;
            if (fixedUpdate != null)
            {
                lock (FixedUpdatesLock)
                {
                    FixedUpdates.Add(fixedUpdate);
                }
            }

            var lateUpdate = target as ILateUpdate;
            if (lateUpdate != null)
            {
                lock (LateUpdatesLock)
                {
                    LateUpdates.Add(lateUpdate);
                }
            }

            var resume = target as IResume;
            var pause = target as IPause;

            if (resume != null)
            {
                if (pause != null)
                {
                    lock (PauseLock)
                    {
                        Pauses.Remove(pause);
                    }
                }

                lock (ResumeLock)
                {
                    Resumes.Add(resume);
                }
            }
        }

        public static void UnRegister(object target)
        {
            if (target == null)
            {
                return;
            }

            var ins = Ins;

            if (ins == null)
            {
                return;
            }

            var update = target as IUpdate;
            if (update != null)
            {
                lock (UpdatesLock)
                {
                    Updates.Remove(update);
                }
            }

            var fixedUpdate = target as IFixedUpdate;
            if (fixedUpdate != null)
            {
                lock (FixedUpdatesLock)
                {
                    FixedUpdates.Remove(fixedUpdate);
                }
            }

            var lateUpdate = target as ILateUpdate;
            if (lateUpdate != null)
            {
                lock (LateUpdatesLock)
                {
                    LateUpdates.Remove(lateUpdate);
                }
            }

            var pause = target as IPause;
            var resume = target as IResume;

            if (pause != null)
            {
                if (resume != null)
                {
                    lock (ResumeLock)
                    {
                        Resumes.Remove(resume);
                    }
                }

                lock (PauseLock)
                {
                    pause.LastPauseTime = Time;
                    Pauses.Add(pause);
                }
            }
        }

        private void Update()
        {
            Time = UnityEngine.Time.time;

            lock (ResumeLock)
            {
                for (var i = Resumes.Count - 1; i >= 0; --i)
                {
                    try
                    {
                        var resume = Resumes[i];

                        if (!ValidCallback(Resumes[i]))
                        {
                            continue;
                        }

                        var lastPauseTime = 0f;
                        var pause = resume as IPause;

                        if (pause != null)
                        {
                            lastPauseTime = pause.LastPauseTime;
                        }

                        resume.OnResume(Time - lastPauseTime);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }

                Resumes.Clear();
            }

            lock (PauseLock)
            {
                for (var i = Pauses.Count - 1; i >= 0; --i)
                {
                    try
                    {
                        if (ValidCallback(Pauses[i]))
                        {
                            Pauses[i].OnPause();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }

                Pauses.Clear();
            }

            lock (UpdatesLock)
            {
                for (var i = Updates.Count - 1; i >= 0; --i)
                {
                    try
                    {
                        if (ValidCallback(Updates[i]) && Updates[i].Enabled)
                        {
                            Updates[i].OnUpdate();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
        }

        private void FixedUpdate()
        {
            lock (FixedUpdatesLock)
            {
                for (var i = FixedUpdates.Count - 1; i >= 0; --i)
                {
                    try
                    {
                        if (ValidCallback(FixedUpdates[i]) && FixedUpdates[i].Enabled)
                        {
                            FixedUpdates[i].OnFixedUpdate();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
        }

        private void LateUpdate()
        {
            lock (LateUpdatesLock)
            {
                for (var i = LateUpdates.Count - 1; i >= 0; --i)
                {
                    try
                    {
                        if (ValidCallback(LateUpdates[i]) && LateUpdates[i].Enabled)
                        {
                            LateUpdates[i].OnLateUpdate();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }
        }

        protected override void OnAwake()
        {
        }

        private static bool ValidCallback(ILifetimeCallback callback)
        {
            if (callback == null)
            {
                return false;
            }

            return (callback as Component) != null;
        }
    }
}