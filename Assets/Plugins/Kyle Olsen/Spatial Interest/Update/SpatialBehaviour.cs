﻿using System.Collections.Generic;
using UnityEngine;

namespace Spatial
{
    public sealed class SpatialBehaviour : MonoBehaviour
    {
        private static readonly List<ILifetimeCallback> CachedCallbacks = new List<ILifetimeCallback>();

        private bool woke;
        private SpatialBinding spatialBinding;
        public bool Updating => SpatialBinding.Updating;

        private SpatialBinding SpatialBinding
        {
            get
            {
                if (spatialBinding == null)
                {
                    spatialBinding = new SpatialBinding(GetComponentInParent<INode>());
                }

                return spatialBinding;
            }
        }

        private void Start()
        {
            OnStartOrEnable();
            woke = true;
        }

        private void OnEnable()
        {
            if (woke)
            {
                SpatialBinding.Enable();
            }
        }

        private void OnStartOrEnable()
        {
            GetComponents(CachedCallbacks);

            for (var i = 0; i < CachedCallbacks.Count; ++i)
            {
                SpatialBinding.RegisterComponentForUpdates(CachedCallbacks[i]);
            }

            SpatialBinding.Enable();
        }

        private void OnDisable()
        {
            SpatialBinding.Disable();
        }
    }
}