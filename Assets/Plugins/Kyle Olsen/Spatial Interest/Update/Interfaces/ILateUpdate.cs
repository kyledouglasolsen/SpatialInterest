﻿namespace Spatial
{
    public interface ILateUpdate : ILifetimeCallback
    {
        bool Enabled { get; }

        void OnLateUpdate();
    }
}