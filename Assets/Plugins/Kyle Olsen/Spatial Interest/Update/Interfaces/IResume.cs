﻿namespace Spatial
{
    public interface IResume : ILifetimeCallback
    {
        void OnResume(float pausedSeconds);
    }
}