﻿namespace Spatial
{
    public interface IUpdate : ILifetimeCallback
    {
        bool Enabled { get; }

        void OnUpdate();
    }
}