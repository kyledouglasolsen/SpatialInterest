﻿namespace Spatial
{
    public interface IFixedUpdate : ILifetimeCallback
    {
        bool Enabled { get; }

        void OnFixedUpdate();
    }
}