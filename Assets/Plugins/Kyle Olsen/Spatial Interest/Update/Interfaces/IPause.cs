﻿namespace Spatial
{
    public interface IPause : ILifetimeCallback
    {
        float LastPauseTime { get; set; }

        void OnPause();
    }
}