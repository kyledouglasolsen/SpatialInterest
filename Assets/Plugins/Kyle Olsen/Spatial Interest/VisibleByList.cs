﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Spatial
{
    public class VisibleByList : IEnumerable<ViewerReference>, IDisposable
    {
        private int revisionNumber;
        private readonly VisibleByList owner;
        private readonly PooledStack<VisibleByList> pool;
        private readonly Dictionary<ICell, NeighborCell> neighbors;
        private readonly FastList<IViewerNode> internalList;

        public VisibleByList(VisibleByList list) : this(list.neighbors, list.internalList, list.revisionNumber)
        {
            owner = list;
        }

        public VisibleByList(Dictionary<ICell, NeighborCell> neighbors, int initialCapacity, int revisionNumber)
        {
            this.revisionNumber = revisionNumber;
            this.neighbors = neighbors;
            internalList = new FastList<IViewerNode>(initialCapacity);
            pool = new PooledStack<VisibleByList>(() => new VisibleByList(this));
        }

        public VisibleByList(Dictionary<ICell, NeighborCell> neighbors, IEnumerable<IViewerNode> collection, int revisionNumber = 0)
        {
            this.revisionNumber = revisionNumber;
            this.neighbors = neighbors;
            internalList = new FastList<IViewerNode>(collection);
            pool = new PooledStack<VisibleByList>(() => new VisibleByList(this));
        }

        public int Count => internalList.Count;

        public void UpdateWith(VisibleByList otherList)
        {
            revisionNumber = otherList.revisionNumber;
            internalList.Clear();

            for (var i = 0; i < otherList.internalList.Count; ++i)
            {
                if (otherList.internalList[i] == null)
                {
                    continue;
                }

                internalList.Add(otherList.internalList[i]);
            }
        }

        public bool TryAdd(IViewerNode node)
        {
            if (internalList.TryAdd(node))
            {
                Interlocked.Increment(ref revisionNumber);
                return true;
            }

            return false;
        }

        public bool Remove(IViewerNode node)
        {
            if (internalList.Remove(node))
            {
                Interlocked.Increment(ref revisionNumber);
                return true;
            }

            return false;
        }

        public bool Contains(IViewerNode node)
        {
            return internalList.Contains(node);
        }

        public void Clear()
        {
            Interlocked.Increment(ref revisionNumber);
            internalList.Clear();
        }

        public VisibleByList Get()
        {
            VisibleByList list = null;

            do
            {
                try
                {
                    list = pool.Pop();
                }
                catch (InvalidOperationException)
                {
                    //ignore thread issue and just try again
                }
            } while (list == null);

            if (!CompareRevisionNumber(list))
            {
                list.UpdateWith(this);
            }

            return list;
        }

        public void Return()
        {
            if (owner != null)
            {
                owner.pool.Push(this);
            }
            else
            {
                Debug.LogError("No owner found for VisibleByList");
            }
        }

        public bool CompareRevisionNumber(VisibleByList otherList)
        {
            return revisionNumber.Equals(otherList.revisionNumber);
        }

        public IEnumerator<ViewerReference> GetEnumerator()
        {
            for (var i = 0; i < internalList.Count; ++i)
            {
                var viewerNode = internalList[i];

                if (viewerNode == null)
                {
                    continue;
                }

                var viewerCell = viewerNode.CurrentCell;

                if (viewerCell == null)
                {
                    continue;
                }

                NeighborCell neighborCell;

                if (!neighbors.TryGetValue(viewerCell, out neighborCell))
                {
                    continue;
                }

                yield return new ViewerReference(viewerNode, neighborCell);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            Return();
        }
    }
}