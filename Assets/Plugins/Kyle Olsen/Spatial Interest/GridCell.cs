﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Spatial
{
    public class GridCell : ICell, IDisposable
    {
        private readonly object localNodesLock = new object();
        private readonly FastList<INode> localNodes = new FastList<INode>(100);
        private readonly object totalVisibleByLock = new object();
        private VisibleByList totalVisibleBy;
        private Dictionary<ICell, NeighborCell> neighbors;
        private int localNodeCount;
        private volatile bool disposed;

        public GridCell(int index, IDivideSpace space, Vector3 center)
        {
            Index = index;
            Space = space;
            Center = center;
        }

        public int Index { get; }
        public IDivideSpace Space { get; }
        public Vector3 Center { get; }
        public int LocalNodeCount => localNodeCount;

        public IEnumerable<NeighborCell> Neighbors
        {
            get
            {
                foreach (var neighbor in neighbors)
                {
                    yield return neighbor.Value;
                }
            }
        }

        public void SetNeighbors(IEnumerable<NeighborCell> neighborCells)
        {
            if (neighbors == null)
            {
                neighbors = new Dictionary<ICell, NeighborCell>();
            }
            else
            {
                neighbors.Clear();
            }

            if (neighborCells != null)
            {
                foreach (var neighborCell in neighborCells)
                {
                    neighbors.Add(neighborCell.Cell, neighborCell);
                }
            }

            lock (totalVisibleByLock)
            {
                if (totalVisibleBy == null)
                {
                    totalVisibleBy = new VisibleByList(neighbors, 10, 0);
                }
                else
                {
                    totalVisibleBy.Clear();
                }
            }
        }

        public void AddNode(INode node)
        {
            if (disposed)
            {
                return;
            }

            bool added;

            lock (localNodesLock)
            {
                added = localNodes.TryAdd(node);
            }

            if (!added)
            {
                return;
            }

            Interlocked.Increment(ref localNodeCount);

            var viewerNode = node as IViewerNode;

            if (viewerNode == null)
            {
                using (var visibleBy = GetVisibleByFromCache())
                {
                    foreach (var visible in visibleBy)
                    {
                        node.OnViewerAdd(visible);
                    }
                }

                return;
            }

            foreach (var neighbor in neighbors)
            {
                neighbor.Key.OnNeighborAddedViewer(viewerNode, this);
            }
        }

        public void RemoveNode(INode node)
        {
            if (disposed)
            {
                return;
            }

            bool removed;

            lock (localNodesLock)
            {
                removed = localNodes.Remove(node);
            }

            if (!removed)
            {
                return;
            }

            Interlocked.Decrement(ref localNodeCount);

            var viewerNode = node as IViewerNode;

            if (viewerNode == null)
            {
                using (var visibleBy = GetVisibleByFromCache())
                {
                    foreach (var visible in visibleBy)
                    {
                        node.OnViewerRemove(visible);
                    }
                }

                return;
            }

            foreach (var neighbor in neighbors)
            {
                neighbor.Key.OnNeighborRemovedViewer(viewerNode, this);
            }
        }

        public void OnNeighborAddedViewer(IViewerNode viewerNode, ICell neighbor)
        {
            bool added;
            var neighborCell = neighbors[neighbor];

            lock (totalVisibleByLock)
            {
                added = totalVisibleBy.TryAdd(viewerNode);
            }

            if (added)
            {
                lock (localNodesLock)
                {
                    foreach (var node in localNodes)
                    {
                        if (node == null)
                        {
                            continue;
                        }

                        node.OnViewerAdd(new ViewerReference(viewerNode, neighborCell));
                    }
                }
            }
        }

        public void OnNeighborRemovedViewer(IViewerNode viewerNode, ICell neighbor)
        {
            bool removed;
            var neighborCell = neighbors[neighbor];

            lock (totalVisibleByLock)
            {
                removed = totalVisibleBy.Remove(viewerNode);
            }

            if (removed)
            {
                lock (localNodesLock)
                {
                    foreach (var node in localNodes)
                    {
                        if (node == null)
                        {
                            continue;
                        }

                        node.OnViewerRemove(new ViewerReference(viewerNode, neighborCell));
                    }
                }
            }
        }

        public VisibleByList GetVisibleByFromCache()
        {
            lock (totalVisibleByLock)
            {
                return totalVisibleBy.Get();
            }
        }

        public void Dispose()
        {
            disposed = true;
            try
            {
                neighbors.Clear();
            }
            catch (Exception)
            {
                //ignore
            }
        }

        public void OnDrawGizmos()
        {
            var size = Vector3.one * Space.Data.CellSize;
            size.y = 0f;
            Gizmos.DrawWireCube(Center, size);
        }
    }
}