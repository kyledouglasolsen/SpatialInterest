﻿namespace Spatial
{
    public struct ViewerReference
    {
        public ViewerReference(IViewerNode viewerNode, NeighborCell neighborCell)
        {
            ViewerNode = viewerNode;
            NeighborCell = neighborCell;
        }

        public IViewerNode ViewerNode { get; }
        public NeighborCell NeighborCell { get; }
    }
}