﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Spatial
{
    public class VisibleNodeGizmos : MonoBehaviour
    {
        private static readonly Vector3[] LodZeroDirections = new[] {Vector3.forward, Vector3.right, Vector3.back, Vector3.left, new Vector3(1f, 0f, 1f), new Vector3(1f, 0f, -1f), new Vector3(-1f, 0f, -1f), new Vector3(-1f, 0f, 1f)};

        [SerializeField] private bool onlyRenderOccupiedCells = true;
        [SerializeField] private bool drawLod = true;
        [SerializeField] private bool drawNeighborNodes = false;
        [SerializeField] private Color[] lodColors;
        private INode node;

        private void OnDrawGizmosSelected()
        {
            if (node == null)
            {
                node = GetComponent<INode>();
            }

            if (node.CurrentCell == null)
            {
                return;
            }

            if (lodColors == null)
            {
                Reset();
            }

            if (drawLod)
            {
                DrawLodGizmos(node.CurrentCell);
            }

            if (drawNeighborNodes)
            {
                DrawVisibleByNodeGizmos(node.CurrentCell);
            }
        }

        private void DrawLodGizmos(ICell currentCell)
        {
            foreach (var neighbor in currentCell.Neighbors)
            {
                if (onlyRenderOccupiedCells && neighbor.Cell.LocalNodeCount < 1)
                {
                    continue;
                }

                Gizmos.color = lodColors[Mathf.Clamp(neighbor.LodLevel, 0, lodColors.Length - 1)];

                var lodScale = (neighbor.LodLevel * 2 - 1);
                var cellSize = currentCell.Space.Data.CellSize;
                var direction = (currentCell.Center - neighbor.Cell.Center).normalized;
                var fromPosition = neighbor.Cell.Center - direction * cellSize / 2f;

                if (neighbor.LodLevel == 0)
                {
                    var halfCellSize = cellSize / 2f;
                    for (var i = 0; i < LodZeroDirections.Length; ++i)
                    {
                        Gizmos.DrawLine(fromPosition + LodZeroDirections[i] * halfCellSize, node.Position);
                    }
                }
                else
                {
                    var ray = new Ray(fromPosition, direction.normalized);
                    var bounds = new Bounds(currentCell.Center, Vector3.one * lodScale * currentCell.Space.Data.CellSize);
                    float distance;

                    if (bounds.IntersectRay(ray, out distance))
                    {
                        Gizmos.DrawLine(fromPosition, fromPosition + ray.direction * distance);
                    }
                    else
                    {
                        Gizmos.DrawRay(ray);
                    }
                }
            }
        }

        private void DrawVisibleByNodeGizmos(ICell currentCell)
        {
            using (var visibleBy = currentCell.GetVisibleByFromCache())
            {
                foreach (var viewer in visibleBy)
                {
                    if (onlyRenderOccupiedCells && viewer.NeighborCell.Cell.LocalNodeCount < 1)
                    {
                        continue;
                    }

                    Gizmos.color = lodColors[Mathf.Clamp(viewer.NeighborCell.LodLevel, 0, lodColors.Length - 1)];

                    if (viewer.NeighborCell.LodLevel == 0)
                    {
                        Gizmos.DrawLine(viewer.ViewerNode.Position, viewer.NeighborCell.Cell.Center);
                    }
                    else
                    {
                        var lodScale = (viewer.NeighborCell.LodLevel * 2 - 1);
                        var direction = (node.Position - viewer.ViewerNode.Position).normalized;
                        var ray = new Ray(viewer.ViewerNode.Position, direction.normalized);
                        var bounds = new Bounds(currentCell.Center, Vector3.one * lodScale * currentCell.Space.Data.CellSize);
                        float distance;

                        if (bounds.IntersectRay(ray, out distance))
                        {
                            Gizmos.DrawLine(viewer.ViewerNode.Position, viewer.ViewerNode.Position + ray.direction * distance);
                        }
                        else
                        {
                            Gizmos.DrawRay(ray);
                        }
                    }
                }
            }
        }

        private void Reset()
        {
            lodColors = new Color[100];

            for (var i = 0; i < lodColors.Length; ++i)
            {
                lodColors[i] = new Color(Random.value, Random.value, Random.value);
            }
        }
    }
}