﻿using System;
using UnityEngine;

public static class GetComponentExtensions
{
    public static Component GetOrAddComponent(this MonoBehaviour monoBehaviour, Type type)
    {
        var get = monoBehaviour.GetComponent(type);

        if ((get == null) && (monoBehaviour.gameObject != null))
            get = monoBehaviour.gameObject.AddComponent(type);

        return get;
    }

    public static T GetOrAddComponent<T>(this MonoBehaviour monoBehaviour) where T : Component
    {
        var get = monoBehaviour.GetComponent<T>();

        if ((get == null) && (monoBehaviour.gameObject != null))
            get = monoBehaviour.gameObject.AddComponent<T>();

        return get;
    }

    public static T[] GetOrAddComponents<T>(this MonoBehaviour monoBehaviour) where T : Component
    {
        var get = monoBehaviour.GetComponents<T>();

        if ((get == null) && (monoBehaviour.gameObject != null))
            get = new T[] {monoBehaviour.gameObject.AddComponent<T>()};

        return get;
    }

    public static Component GetOrAddComponent(this GameObject gameObject, Type type)
    {
        var get = gameObject.GetComponent(type);

        if ((get == null) && (gameObject != null))
            get = gameObject.AddComponent(type);

        return get;
    }

    public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
    {
        var get = gameObject.GetComponent<T>();

        if (get == null)
            get = gameObject.AddComponent<T>();

        return get;
    }

    public static T[] GetOrAddComponents<T>(this GameObject gameObject) where T : Component
    {
        var get = gameObject.GetComponents<T>();

        if (get == null)
            get = new T[] {gameObject.AddComponent<T>()};

        return get;
    }

    public static Component GetOrAddComponent(this Component component, Type type)
    {
        var get = component.GetComponent(type);

        if ((get == null) && (component != null))
            get = component.gameObject.AddComponent(type);

        return get;
    }

    public static T GetOrAddComponent<T>(this Component component) where T : Component
    {
        var get = component.GetComponent<T>();

        if (get == null)
            get = component.gameObject.AddComponent<T>();

        return get;
    }

    public static T[] GetOrAddComponents<T>(this Component component) where T : Component
    {
        var get = component.GetComponents<T>();

        if (get.Length < 1)
            get = new T[] {component.gameObject.AddComponent<T>()};

        return get;
    }
}