﻿using System;
using System.Collections;
using System.Collections.Generic;

public class FastList<T> : IList<T>
{
    private static T Default = default(T);

    private CustomEnumerator enumerator;
    private bool isEnumeratorRetained;

    private readonly List<T> list;
    private readonly Dictionary<int, int> dictionary;
    private readonly PooledStack<int> containerPool;

    public FastList() : this(0)
    {
    }

    public FastList(IEnumerable<T> enumerable)
    {
        list = new List<T>();
        dictionary = new Dictionary<int, int>();
        containerPool = new PooledStack<int>(() => -1);

        foreach (var item in enumerable)
        {
            if (item == null)
            {
                continue;
            }

            Add(item);
        }
    }

    public FastList(ICollection<T> collection)
    {
        list = new List<T>(collection.Count);
        dictionary = new Dictionary<int, int>();
        containerPool = new PooledStack<int>(() => -1);

        foreach (var item in collection)
        {
            if (item == null)
            {
                continue;
            }

            Add(item);
        }
    }

    public FastList(int capacity)
    {
        list = new List<T>(capacity);
        dictionary = new Dictionary<int, int>(capacity);
        containerPool = new PooledStack<int>(() => -1, capacity);
    }

    public T this[int index]
    {
        get { return list[index]; }
        set { Insert(index, value); }
    }

    public int Capacity
    {
        get { return list.Capacity; }
        set { list.Capacity = value; }
    }

    public int Count => list.Count;
    public int ManagedCount { get; private set; }
    public bool IsReadOnly => false;

    public IEnumerator<T> GetEnumerator()
    {
        return RetainEnumerator();
    }

    public T First()
    {
        for (var i = 0; i < list.Count; ++i)
        {
            if (list[i] == null)
            {
                continue;
            }

            return list[i];
        }

        return Default;
    }

    public void Add(T item)
    {
        TryAdd(item);
    }

    public bool TryAdd(T item)
    {
        var hashCode = item.GetHashCode();

        if (dictionary.ContainsKey(hashCode))
        {
            return false;
        }

        var index = containerPool.Pop();

        if (index == -1)
        {
            list.Add(item);
            index = list.Count - 1;
        }
        else
        {
            list[index] = item;
        }

        ++ManagedCount;
        dictionary[hashCode] = index;

        return true;
    }

    public void AddRange(IEnumerable<T> collection)
    {
        foreach (var item in collection)
        {
            if (item == null)
            {
                continue;
            }

            Add(item);
        }
    }

    public int BinarySearch(T item)
    {
        return list.BinarySearch(item);
    }

    public int BinarySearch(T item, IComparer<T> comparer)
    {
        return list.BinarySearch(item, comparer);
    }

    public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
    {
        return list.BinarySearch(index, count, item, comparer);
    }

    public void Clear()
    {
        list.Clear();
        containerPool.Clear();
        dictionary.Clear();
        ManagedCount = 0;
    }

    public bool Contains(T item)
    {
        return dictionary.ContainsKey(item.GetHashCode());
    }

    public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
    {
        return list.ConvertAll(converter);
    }

    public void CopyTo(T[] array)
    {
        list.CopyTo(array);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        list.CopyTo(array, arrayIndex);
    }

    public void CopyTo(int index, T[] array, int arrayIndex, int count)
    {
        list.CopyTo(index, array, arrayIndex, count);
    }

    public bool Exists(Predicate<T> match)
    {
        return list.Exists(match);
    }

    public T Find(Predicate<T> match)
    {
        return list.Find(match);
    }

    public List<T> FindAll(Predicate<T> match)
    {
        return list.FindAll(match);
    }

    public int FindIndex(Predicate<T> match)
    {
        return list.FindIndex(match);
    }

    public int FindIndex(int startIndex, Predicate<T> match)
    {
        return list.FindIndex(startIndex, match);
    }

    public int FindIndex(int startIndex, int count, Predicate<T> match)
    {
        return list.FindIndex(startIndex, count, match);
    }

    public T FindLast(Predicate<T> match)
    {
        return list.FindLast(match);
    }

    public int FindLastIndex(Predicate<T> match)
    {
        return list.FindLastIndex(match);
    }

    public int FindLastIndex(int startIndex, Predicate<T> match)
    {
        return list.FindLastIndex(startIndex, match);
    }

    public int FindLastIndex(int startIndex, int count, Predicate<T> match)
    {
        return list.FindLastIndex(startIndex, count, match);
    }

    public void ForEach(Action<T> action)
    {
        list.ForEach(action);
    }

    public List<T> GetRange(int index, int count)
    {
        return list.GetRange(index, count);
    }

    public int IndexOf(T item)
    {
        return dictionary[item.GetHashCode()];
    }

    public void Insert(int index, T item)
    {
        throw new NotSupportedException();
    }

    public int LastIndexOf(T item)
    {
        return dictionary[item.GetHashCode()];
    }

    public int LastIndexOf(T item, int index)
    {
        return dictionary[item.GetHashCode()];
    }

    public int LastIndexOf(T item, int index, int count)
    {
        return dictionary[item.GetHashCode()];
    }

    public bool Remove(T item)
    {
        var hashCode = item.GetHashCode();
        int index;

        if (dictionary.TryGetValue(hashCode, out index))
        {
            if (index > -1 && index < Count)
            {
                list[index] = Default;
            }

            --ManagedCount;
            dictionary.Remove(hashCode);
            containerPool.Push(index);

            return true;
        }

        return false;
    }

    public int RemoveAll(Predicate<T> match)
    {
        var items = list.FindAll(match);

        for (var i = items.Count - 1; i >= 0; --i)
        {
            if (items[i] == null)
            {
                continue;
            }

            Remove(items[i]);
        }

        return items.Count;
    }

    public void RemoveAt(int index)
    {
        var item = list[index];

        if (item != null)
        {
            Remove(item);
        }
    }

    public void RemoveRange(int index, int count)
    {
        for (var i = list.Count - 1; i >= 0; --i)
        {
            if (list[i] != null)
            {
                Remove(list[i]);
            }
        }
    }

    public void Reverse()
    {
        list.Reverse();
    }

    public void Reverse(int index, int count)
    {
        list.Reverse(index, count);
    }

    public void Sort()
    {
        list.Sort();
    }

    public void Sort(IComparer<T> comparer)
    {
        list.Sort(comparer);
    }

    public void Sort(Comparison<T> comparison)
    {
        list.Sort(comparison);
    }

    public void Sort(int index, int count, IComparer<T> comparer)
    {
        list.Sort(index, count, comparer);
    }

    public T[] ToArray()
    {
        return list.ToArray();
    }

    public void TrimExcess()
    {
        list.TrimExcess();
    }

    public bool TrueForAll(Predicate<T> match)
    {
        return list.TrueForAll(match);
    }

    private IEnumerator<T> RetainEnumerator()
    {
        if (enumerator == null)
        {
            enumerator = new CustomEnumerator(this);
            isEnumeratorRetained = true;
            return enumerator;
        }

        if (isEnumeratorRetained)
        {
            return GetEnumerator();
        }
        else
        {
            isEnumeratorRetained = true;
            enumerator.MyReset();
            return enumerator;
        }
    }

    private void ReleaseEnumerator(CustomEnumerator customEnumerator)
    {
        if (ReferenceEquals(customEnumerator, enumerator))
        {
            isEnumeratorRetained = false;
        }
    }

    private List<T>.Enumerator GetBaseEnumerator()
    {
        return list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public class CustomEnumerator : IEnumerator<T>, System.Collections.IEnumerator
    {
        private FastList<T> list;
        private List<T>.Enumerator enumerator;

        public CustomEnumerator(FastList<T> list)
        {
            this.list = list;
            enumerator = list.GetBaseEnumerator();
        }

        public void MyReset()
        {
            enumerator = list.GetBaseEnumerator();
        }

        bool IEnumerator.MoveNext()
        {
            return enumerator.MoveNext();
        }

        void IEnumerator.Reset()
        {
            enumerator = list.GetBaseEnumerator();
        }

        object IEnumerator.Current => this.enumerator.Current;

        void IDisposable.Dispose()
        {
            enumerator.Dispose();
            list.ReleaseEnumerator(this);
        }

        T IEnumerator<T>.Current => this.enumerator.Current;
    }
}