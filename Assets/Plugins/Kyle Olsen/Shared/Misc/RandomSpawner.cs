﻿using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    [SerializeField] private GameObject prefab = null;
    [SerializeField] private float radius = 100f;
    [SerializeField] private int count = 100;

    private int remainingToSpawn;

    private void Awake()
    {
        remainingToSpawn = count;
    }

    private void Update()
    {
        if (prefab == null)
        {
            remainingToSpawn = 0;
        }

        var position = transform.position;
        var halfRadius = radius;
        var endTarget = Mathf.Clamp(remainingToSpawn - 1000, 0, count);

        for (; remainingToSpawn > endTarget; --remainingToSpawn)
        {
            var random = Random.insideUnitSphere * halfRadius;
            random.y = 0f;
            Instantiate(prefab, position + random, Quaternion.identity);
        }

        if (remainingToSpawn == 0)
        {
            enabled = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.localPosition, radius);
    }
}